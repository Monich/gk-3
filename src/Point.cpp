#include "Point.hpp"

#include "Color.hpp"
#include "Image.hpp"

#include <math.h>

Point::Point() : x(0), y(0)
{
}

Point::Point(int x, int y) : x(x), y(y)
{
}

void Point::DrawLine(Image& i, Point * other)
{
	int y_diff = abs(other->y - y);
	int x_diff = abs(other->x - x);

	if (y_diff > x_diff)
		_PrepareDrawLineY(i, other);
	else _PrepareDrawLineX(i, other);
}

void Point::_PrepareDrawLineX(Image & i, Point * other)
{
	if (other->x < x)
		other->_DrawLineX(i, this);
	else _DrawLineX(i, other);
}

void Point::_PrepareDrawLineY(Image & i, Point * other)
{
	if (other->y < y)
		other->_DrawLineY(i, this);
	else _DrawLineY(i, other);
}

void Point::_DrawLineX(Image& i, Point * other)
{
	int curr_x = x;
	int curr_y = y;
	int epsilon = 0;
	int deltaEps = other->y - curr_y;
	int r = (other->x - x) / 2;
	Color c(255);
	int y_direction;

	if (deltaEps >= 0)
		y_direction = 1;
	else y_direction = -1;

	i.DrawPixel(curr_x, curr_y, c);

	while (curr_x <= other->x)
	{
		curr_x++;
		epsilon += abs(deltaEps);

		if (epsilon > r) {
			epsilon -= (other->x - x);
			curr_y += y_direction;
		}

		i.DrawPixel(curr_x, curr_y, c);
	}
}

void Point::_DrawLineY(Image & i, Point * other)
{
	int curr_x = x;
	int curr_y = y;
	int epsilon = 0;
	int deltaEps = other->x - curr_x;
	int r = (other->y - y) / 2;
	Color c(255);
	int x_direction;

	if (deltaEps >= 0)
		x_direction = 1;
	else x_direction = -1;

	i.DrawPixel(curr_x, curr_y, c);

	while (curr_y <= other->y)
	{
		curr_y++;
		epsilon += abs(deltaEps);

		if (epsilon > r) {
			epsilon -= (other->y - y);
			curr_x += x_direction;
		}

		i.DrawPixel(curr_x, curr_y, c);
	}
}
