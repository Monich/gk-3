#include "Polygon.hpp"

#include "Point.hpp"
#include "Transformation.hpp"

Polygon::~Polygon()
{
	for (auto it = begin(); it != end(); it++)
		delete *it;
}

void Polygon::Load(FILE * f)
{
	bool cont = true;
	char c;
	int x, y;
	int hits = fscanf(f, "%c", &c);

	if (hits != 1 || c != 'W')
		return;

	do
	{
		hits = fscanf(f, "%d %d", &x, &y);
		if (hits == 2)
		{
			Point* p = new Point(x, y);
			push_back(p);
		}
		else cont = false;
	} while (cont);
}

void Polygon::Draw(Image & i, Transformation & t)
{
	Point* first = nullptr;
	Point* second = t.Transform(back());
	for (auto it = begin(); it != end(); it++)
	{
		delete first;
		first = second;
		second = t.Transform(*it);
		first->DrawLine(i, second);
	}
}
