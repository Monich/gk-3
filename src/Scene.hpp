#pragma once

#include <list>

class Image;
class Polygon;
class Transformation;

class Scene : public std::list<Polygon*>
{
public:
	~Scene();

	bool Load(const char* sceneFile);

	void Draw(Image& i, Transformation& t);
};
