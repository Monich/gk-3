#pragma once

class Color;
class Transformation;

class Image
{
public:
	Image(unsigned width, unsigned height);
	~Image();

	bool Save(const char * filename);

	void DrawPixel(unsigned x, unsigned y, Color& c);

	void NearestPixelInterpolation(Image* input, Transformation& t);
	void LinearInterpolation(Image* input, Transformation& t);
	void BicubicInterpolation(Image* input, Transformation& t);

	unsigned GetWidth() const;
	unsigned GetHeight() const;

	static Image* LoadFromFile(const char * filename);

private:

	Color GetNearestPixel(float x, float y);
	Color GetLinear(float x, float y);
	Color GetBicubic(float x, float y);
	Color GetCubic(int x, float y);

	Color GetPoint(int x, int y);

	Color* pixels;
	const unsigned width;
    const unsigned height;
};
