#pragma once

class Color
{
public:
	Color();
	Color(unsigned char color);

	void SetBlack();

	Color& operator=(const Color& c);

	Color SolveLinear(Color other, float alpha);
	static Color SolveCubic(Color pixels[4], float alpha);

private:
	unsigned char rgb[3];
};
