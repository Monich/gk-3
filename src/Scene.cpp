#include "Scene.hpp"

#include "Polygon.hpp"

#include <stdio.h>

Scene::~Scene()
{
	for (auto it = begin(); it != end(); it++)
		delete *it;
}

bool Scene::Load(const char * sceneFile)
{
	FILE* file = fopen(sceneFile, "r");
	if (!file)
		return false;

	while (!feof(file))
	{
		Polygon* p = new Polygon();
		p->Load(file);
		push_back(p);
	}

	fclose(file);
	return true;
}

void Scene::Draw(Image & i, Transformation & t)
{
	for (auto it = begin(); it != end(); it++)
		(*it)->Draw(i, t);
}
