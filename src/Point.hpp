#pragma once

class Image;

class Point
{
public:
	Point();
	Point(int x, int y);

	void DrawLine(Image& i, Point* other);

	const int x, y;

private:
	void _PrepareDrawLineX(Image& i, Point* other);
	void _PrepareDrawLineY(Image& i, Point* other);

	void _DrawLineX(Image& i, Point* other);
	void _DrawLineY(Image& i, Point* other);
};
