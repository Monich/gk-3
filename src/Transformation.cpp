#include "Transformation.hpp"

#include "Point.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>

Transformation::Transformation() : matrix(1)
{
}

bool Transformation::Load(const char * filename)
{
	FILE* file = fopen(filename, "r");

	if (!file)
		return false;

	char operationType;
	int hit;
	bool success;

	while (!feof(file))
	{
		hit = fscanf(file, "%c ", &operationType);
		if (hit == 0)
			return false;

		switch (operationType)
		{
		case 'T':
		case 't':
			success = _Transform(file);
			break;
		case 'S':
		case 's':
			success = _Scale(file);
			break;
		case 'R':
		case 'r':
			success = _Rotate(file);
			break;
		default:
			success = false;
		}

		if (success != true)
			return false;
	}

	return true;
}

Point * Transformation::Transform(Point * original)
{
	glm::vec4 point(original->x, original->y, 0, 1);
	point = matrix * point;
	return new Point(point.x, point.y);
}

glm::vec4 Transformation::Transform(unsigned x, unsigned y)
{
	glm::vec4 point(x, y, 0, 1);
	point = matrix * point;
	return point;
}

void Transformation::Reverse()
{
	matrix = glm::inverse(matrix);
}

bool Transformation::_Transform(FILE * file)
{
	float x, y;
	int hit = fscanf(file, "%f %f ", &x, &y);
	if (hit != 2)
		return false;

	matrix = glm::translate(glm::mat4(1), glm::vec3(x, y, 0)) * matrix;

	return true;
}

bool Transformation::_Scale(FILE * file)
{
	float x, y;
	int hit = fscanf(file, "%f %f ", &x, &y);
	if (hit != 2)
		return false;

	matrix = glm::scale(glm::mat4(1), glm::vec3(x, y, 1)) * matrix;

	return true;
}

bool Transformation::_Rotate(FILE * file)
{
	float alpha;
	int hit = fscanf(file, "%f ", &alpha);
	if (hit != 1)
		return false;

	alpha = alpha * M_PI / 180;
	matrix = glm::rotate(glm::mat4(1), alpha, glm::vec3(0, 0, 1)) * matrix;

	return true;
}
