#pragma once

#include <list>
#include <stdio.h>

class Image;
class Point;
class Transformation;

class Polygon : public std::list<Point*>
{
public:
	~Polygon();

	void Load(FILE* f);

	void Draw(Image& i, Transformation& t);
};
