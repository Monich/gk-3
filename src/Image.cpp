#include "Image.hpp"

#include "Color.hpp"
#include "Transformation.hpp"

#include "IL/il.h"
#include "IL/ilu.h"

#include <string.h>

Image::Image(unsigned width, unsigned height) : width(width), height(height)
{
	pixels = new Color[width * height];
}

Image::~Image()
{
	delete[] pixels;
}

bool Image::Save(const char * filename)
{
	ILuint imageId = ilGenImage();
	ilBindImage(imageId);
	ilTexImage(width, height, 1, 3, IL_RGB, IL_UNSIGNED_BYTE, pixels);

	ilEnable(IL_FILE_OVERWRITE);
	bool result = ilSaveImage(filename);

	if (!result)
	{
		ILenum err = ilGetError();
		const char * errString = iluErrorString(err);
		printf("err: %s\n", errString);
	}

	ilDeleteImage(imageId);

	return result;
}

void Image::DrawPixel(unsigned x, unsigned y, Color& c)
{
	if(x >= 0 && y >= 0 && x < width && y < height)
		pixels[y*width + x] = c;
}

void Image::NearestPixelInterpolation(Image * input, Transformation & t)
{
	Color c;
	glm::vec4 p;
	for (unsigned w = 0; w < width; w++)
		for (unsigned h = 0; h < height; h++)
		{
			p = t.Transform(w, h);

			c = input->GetNearestPixel(p.x, p.y);
			DrawPixel(w, h, c);
		}
}

void Image::LinearInterpolation(Image * input, Transformation & t)
{
	Color c;
	glm::vec4 p;
	for (unsigned w = 0; w < width; w++)
		for (unsigned h = 0; h < height; h++)
		{
			p = t.Transform(w, h);

			c = input->GetLinear(p.x, p.y);
			DrawPixel(w, h, c);
		}
}

void Image::BicubicInterpolation(Image * input, Transformation & t)
{
	Color c;
	glm::vec4 p;
	for (unsigned w = 0; w < width; w++)
		for (unsigned h = 0; h < height; h++)
		{
			p = t.Transform(w, h);

			c = input->GetBicubic(p.x, p.y);
			DrawPixel(w, h, c);
		}
}

unsigned Image::GetWidth() const
{
	return width;
}

unsigned Image::GetHeight() const
{
	return height;
}

Image * Image::LoadFromFile(const char * filename)
{
	ILuint imageId = ilGenImage();
	ilBindImage(imageId);
	bool result = ilLoadImage(filename);

	if (!result)
	{
		ILenum err = ilGetError();
		const char * errString = iluErrorString(err);
		printf("err: %s\n", errString);
		return nullptr;
	}

	ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);

	int width = ilGetInteger(IL_IMAGE_WIDTH);
	int height = ilGetInteger(IL_IMAGE_HEIGHT);
	Image* i = new Image(width, height);

	ilCopyPixels(0, 0, 0, width, height, 1, IL_RGB, IL_UNSIGNED_BYTE, i->pixels);

	ilDeleteImage(imageId);

	return i;
}

Color Image::GetNearestPixel(float x, float y)
{
	if (x >= 0 && y >= 0)
	{
		unsigned w = unsigned(x);
		unsigned h = unsigned(y);
		if (w >= 0 && w < width && h >= 0 && h < height)
			return pixels[h*width + w];
	}
	return Color();
}

Color Image::GetLinear(float x, float y)
{
	int floorx = int(x);
	int floory = int(y);
	Color points[4];

	// 1 - nominate points

	points[0] = GetPoint(floorx, floory);
	points[1] = GetPoint(floorx + 1, floory);
	points[2] = GetPoint(floorx, floory + 1);
	points[3] = GetPoint(floorx + 1, floory + 1);

	// 2 - Solve linears
	Color subpixel[2];
	subpixel[0] = points[0].SolveLinear(points[1], x - floorx);
	subpixel[1] = points[2].SolveLinear(points[3], x - floorx);

	return subpixel[0].SolveLinear(subpixel[1], y - floory);
}

Color Image::GetBicubic(float x, float y)
{
	int floorx = int(x);
	Color points[4];

	// 1 - Get subpixels

	points[0] = GetCubic(floorx - 1, y);
	points[1] = GetCubic(floorx, y);
	points[2] = GetCubic(floorx + 1, y);
	points[3] = GetCubic(floorx + 2, y);

	return Color::SolveCubic(points, x - floorx);
}

Color Image::GetCubic(int x, float y)
{
	int floory = int(y);
	Color points[4];

	// 1 - Get subpixels

	points[0] = GetPoint(x, floory - 1);
	points[1] = GetPoint(x, floory);
	points[2] = GetPoint(x, floory + 1);
	points[3] = GetPoint(x, floory + 2);

	return Color::SolveCubic(points, y - floory);
}

Color Image::GetPoint(int x, int y)
{
	if (x >= 0 && y >= 0 && x < width && y < height)
		return pixels[y*width + x];
	else return Color();
}
