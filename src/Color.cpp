#include "Color.hpp"

#include <algorithm>

Color::Color()
{
	rgb[0] = rgb[1] = rgb[2] = 0;
}

Color::Color(unsigned char color)
{
	rgb[0] = rgb[1] = rgb[2] = color;
}

void Color::SetBlack()
{
	rgb[0] = rgb[1] = rgb[2];
}

Color& Color::operator=(const Color & c)
{
	rgb[0] = c.rgb[0];
	rgb[1] = c.rgb[1];
	rgb[2] = c.rgb[2];
}

Color Color::SolveLinear(Color other, float alpha)
{
	Color c;
	c.rgb[0] = rgb[0] * (1 - alpha) + other.rgb[0] * alpha;
	c.rgb[1] = rgb[1] * (1 - alpha) + other.rgb[1] * alpha;
	c.rgb[2] = rgb[2] * (1 - alpha) + other.rgb[2] * alpha;
	return c;
}

Color Color::SolveCubic(Color p[4], float x)
{
	Color c;
	float extendedRgb[3];

	extendedRgb[0] = float(p[1].rgb[0]) + 0.5 * x*( float(p[2].rgb[0]) - float(p[0].rgb[0]) + x * (2.0 * float(p[0].rgb[0]) - 5.0* float(p[1].rgb[0]) + 4.0* float(p[2].rgb[0])
		- float (p[3].rgb[0]) + x * (3.0*( float(p[1].rgb[0]) - float(p[2].rgb[0])) + float(p[3].rgb[0]) - float(p[0].rgb[0]))));

	extendedRgb[1] = float(p[1].rgb[1]) + 0.5 * x*(float(p[2].rgb[1]) - float(p[0].rgb[1]) + x * (2.0 * float(p[0].rgb[1]) - 5.0* float(p[1].rgb[1]) + 4.0* float(p[2].rgb[1])
		- float(p[3].rgb[1]) + x * (3.0*(float(p[1].rgb[1]) - float(p[2].rgb[1])) + float(p[3].rgb[1]) - float(p[0].rgb[1]))));

	extendedRgb[2] = float(p[1].rgb[2]) + 0.5 * x*(float(p[2].rgb[2]) - float(p[0].rgb[2]) + x * (2.0 * float(p[0].rgb[2]) - 5.0* float(p[1].rgb[2]) + 4.0* float(p[2].rgb[2])
		- float(p[3].rgb[2]) + x * (3.0*(float(p[1].rgb[2]) - float(p[2].rgb[2])) + float(p[3].rgb[2]) - float(p[0].rgb[2]))));

	c.rgb[0] = (unsigned char)(std::max(0.f, std::min(extendedRgb[0], 255.f)));
	c.rgb[1] = (unsigned char)(std::max(0.f, std::min(extendedRgb[1], 255.f)));
	c.rgb[2] = (unsigned char)(std::max(0.f, std::min(extendedRgb[2], 255.f)));
	return c;
}
