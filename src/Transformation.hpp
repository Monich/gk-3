#pragma once

#include <glm/glm.hpp>

#include <stdio.h>

class Point;

class Transformation
{
public:
	Transformation();

	bool Load(const char* filename);

	Point* Transform(Point* original);

	glm::vec4 Transform(unsigned x, unsigned y);

	void Reverse();
private:
	glm::mat4 matrix;

	bool _Transform(FILE* file);
	bool _Scale(FILE* file);
	bool _Rotate(FILE* file);
};
