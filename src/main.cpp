#include "Image.hpp"
#include "Scene.hpp"
#include "Transformation.hpp"

#include <IL/il.h>
#include <IL/ilu.h>

int main(int argc, const char** args)
{
	if (argc == 2 && !strcmp(args[1], "help"))
	{
		printf("Usage:\n"
			"gk3 <input scene> <transformation file> <output width> <output height> <output file>\n"
			"gk3 <input image> <transformation file> <output width> <output height> <output file> <interpolation method>\n");
		return 0;
	}

	bool raster = argc == 7;
	bool vector = argc == 6;

	if (!(raster || vector))
	{
		return 1;
	}

	ilInit();
	iluInit();

	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
	ilEnable(IL_ORIGIN_SET);

	Scene scene;
	Image* originalImage = nullptr;;

	if (vector)
	{
		printf("Loading scene from file %s\n", args[1]);
		
		if (!scene.Load(args[1]))
			return 1;
	}
	else
	{
		printf("Loading input image from file %s\n", args[1]);

		originalImage = Image::LoadFromFile(args[1]);
		if (!originalImage)
		{
			delete originalImage;
			return 1;
		}
	}

	printf("Reading transformation\n");
	Transformation t;

	if (!t.Load(args[2]))
	{
		printf("Transformation file missing or corrupted\n");
		delete originalImage;
		return 1;
	}

	printf("Constructing image\n");
	unsigned width, height;
	
	int hits = 0;
	hits += sscanf(args[3], "%u", &width);
	hits += sscanf(args[4], "%u", &height);

	if (hits != 2)
	{
		printf("Width and height malformed, exiting...\n");
		delete originalImage;
		return 1;
	}

	Image image(width, height);

	if (vector)
	{
		scene.Draw(image, t);
	}
	else
	{
		t.Reverse();
		switch (args[6][0])
		{
		case 'n': // Nearest
			image.NearestPixelInterpolation(originalImage, t);
			break;
		case 'l': // Linear
			image.LinearInterpolation(originalImage, t);
			break;
		case 'b': // Bicubic
			image.BicubicInterpolation(originalImage, t);
			break;
		default:
			printf("Invalid operation %c\n", args[6][0]);
			return 1;
		}
	}

	printf("Saving image\n");
	if (!image.Save(args[5]))
	{
		printf("Image saving failed!\n");
		delete originalImage;
		return 1;
	}

	printf("Image saved\n");

	delete originalImage;
	return 0;
}
