FIND_PATH(GLM_INCLUDE_DIR
	NAMES glm/glm.hpp
	PATHS
    deps/include
    /usr/include
    "C:/Program Files/glm"
  DOC "The directory containing glm headers."
)

if(GLM_INCLUDE_DIR)
    set(${GLM_FOUND} 1)
	include_directories(${GLM_INCLUDE_DIR})
else(GLM_INCLUDE_DIR)
    message(FATAL_ERROR "Couldn't locate GLM library")
endif(GLM_INCLUDE_DIR)

